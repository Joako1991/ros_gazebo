#!/usr/bin/zsh

sudo apt-get update && sudo apt-get install -y \
    ros-melodic-joy \
    ros-melodic-teleop-twist-joy \
    ros-melodic-teleop-twist-keyboard \
    ros-melodic-laser-proc \
    ros-melodic-rgbd-launch \
    ros-melodic-depthimage-to-laserscan \
    ros-melodic-rosserial-arduino \
    ros-melodic-rosserial-python \
    ros-melodic-rosserial-server \
    ros-melodic-rosserial-client \
    ros-melodic-rosserial-msgs \
    ros-melodic-amcl \
    ros-melodic-map-server \
    ros-melodic-move-base \
    ros-melodic-urdf \
    ros-melodic-xacro \
    ros-melodic-compressed-image-transport \
    ros-melodic-rqt-image-view \
    ros-melodic-gmapping \
    ros-melodic-navigation \
    ros-melodic-interactive-markers

sudo apt-get install -y ros-melodic-effort-controllers \
    ros-melodic-joy ros-melodic-teleop-twist-joy \
    ros-melodic-teleop-twist-keyboard ros-melodic-laser-proc \
    ros-melodic-rgbd-launch ros-melodic-depthimage-to-laserscan \
    ros-melodic-rosserial-arduino ros-melodic-rosserial-python \
    ros-melodic-rosserial-server ros-melodic-rosserial-client \
    ros-melodic-rosserial-msgs ros-melodic-amcl ros-melodic-map-server \
    ros-melodic-move-base ros-melodic-urdf ros-melodic-xacro \
    ros-melodic-compressed-image-transport ros-melodic-rqt-image-view \
    ros-melodic-gmapping ros-melodic-navigation ros-melodic-interactive-markers

sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'
wget http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -
sudo apt-get update
sudo apt-get install libignition-math4-dev libgazebo9-dev -y
sudo apt upgrade -y libignition-math2

source ~/.zshrc
sudo apt-get update && sudo apt-get install -y\
    ros-melodic-turtlebot3-msgs \
    ros-melodic-turtlebot3 \
    ros-melodic-hokuyo3d \
    ros-melodic-joint-trajectory-controller ros-melodic-yocs-virtual-sensor

(
    roscd
    cd ../src
    git clone -b melodic-devel https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git
    roscd
    cd ..
    catkin_make
)
rospack profile
echo 'export TURTLEBOT3_MODEL=burger' >> ~/.zshrc
echo 'export GAZEBO_MODEL_DATABASE_URI=http://models.gazebosim.org/' >> ~/.zshrc
echo 'alias tb3_empty="roslaunch turtlebot3_gazebo turtlebot3_empty_world.launch"' >> ~/.zshrc
echo 'alias tb3_maze="roslaunch turtlebot3_gazebo turtlebot3_world.launch"' >> ~/.zshrc
echo 'alias tb3_house="roslaunch turtlebot3_gazebo turtlebot3_house.launch"' >> ~/.zshrc

# Important command when dependencies are missing:
rosdep install --from-paths src --ignore-src -r -y

source ~/.zshrc
